﻿using System.Windows;

namespace WpfFifteens
{
    /// <summary>
    /// Interaction logic for AddNewScore.xaml
    /// </summary>
    public partial class AddNewScore : Window
    {
        public int Score { get; set; }
        public AddNewScore()
        {
            InitializeComponent();
            Loaded += (s,e) => score.Content = $"Your score is {Score}";
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            string name = CName.Text;
            if (TestName(name))
            {
                using (var db = new GameDbContext())
                {
                    var users = db.Highscores.Add(new Highscore { Name = CName.Text, Score = Score });
                    db.SaveChanges();
                }
                HighscoresDialog highscoresDialog = new HighscoresDialog();
                highscoresDialog.ShowDialog();
            }
            Close();
        }

        private bool TestName(string name)
        {
            if (name.Length < 3)
            {
                MessageBox.Show("Name must contain minimum 3 letters.", "Error!");
                return false;
            }
            if (name.Length >= 25)
            {
                MessageBox.Show("Name must contain maximum 25 letters.", "Error!");
                return false;
            }
            return true;
        }
    }
}
