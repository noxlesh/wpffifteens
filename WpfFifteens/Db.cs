﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using SQLite.CodeFirst;

namespace WpfFifteens
{
    [Table("Highscores")]
    class Highscore
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int Score { get; set; }
    }

    class GameDbContext : DbContext
    {
        public virtual DbSet<Highscore> Highscores { get; set; }

        public GameDbContext()
            : base("SQLiteConnection")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<GameDbContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);

        }
    }
}
