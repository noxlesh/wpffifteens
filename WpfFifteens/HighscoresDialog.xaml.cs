﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace WpfFifteens
{
    /// <summary>
    /// Interaction logic for Highscores.xaml
    /// </summary>
    public partial class HighscoresDialog : Window
    {
        public HighscoresDialog()
        {
            InitializeComponent();
            Loaded += Window_Loaded;
        }

        private void Window_Loaded(object sender, EventArgs e)
        {
            using (var db = new GameDbContext())
            {
                List<Highscore> hl = db.Highscores.ToList();
                ResultsList.ItemsSource = hl;
                // sorting by column
                //collectionview view = (collectionview)collectionviewsource.getdefaultview(resultslist.itemssource);
                //view.sortdescriptions.add(new sortdescription("score", listsortdirection.descending));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
