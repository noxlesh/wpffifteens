﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Collections;

namespace WpfFifteens
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point emptyBlk = new Point(3, 3);
        private int steps;
        public MainWindow()
        {
            InitializeComponent();
            InitGame();
        }

        /// <summary>
        /// Shuffles the blocks by changing Content property
        /// </summary>
        private void InitGame()
        {
            foreach (Block blk in grid.Children)
            {
                blk.Click += OnBlockClick;
            }
            int[] getSeq = new int[15];
            do {
                List<int> items = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
                IList blocks = grid.Children;
                for (int i = 0; i < blocks.Count; i++)
                {
                    var rnd = new Random(DateTime.Now.Millisecond);
                    var blk = (Block)blocks[i];

                    int item = items[rnd.Next(items.Count - 1)];
                    items.Remove(item);
                    getSeq[GetOneRowPos(blk)] = item;
                    blk.Content = item.ToString();
                }
            } while (!Resolvable(getSeq));
        }

        /// <summary>
        /// Tests whether the generated sequence can be resolvable
        /// </summary>
        /// <param name="genSeq"></param>
        /// <returns></returns>
        private bool Resolvable(int[] genSeq)
        {
            int result = 0;
            for (int i = 0; i < genSeq.Length; i++)
            {
                int curr = genSeq[i];
                int j = i + 1;
                int count = 0;
                while (j < genSeq.Length)
                {
                    if (genSeq[j] < curr)
                        count++;
                    j++;
                }
                result += count;
            }
            if (result % 2 == 0) return true; // even
            return false; // odd
        }

        /// <summary>
        /// Checks if the block can move then swaps it with the emptyBlock
        /// </summary>
        /// <param name="blk">The block to move</param>
        private void CheckSwap(Block blk)
        {
            int x = Grid.GetColumn(blk);
            int y = Grid.GetRow(blk);
            if (new Point(x+1,y).Equals(emptyBlk)) //check right
            {
                Swap(blk);
            }
            if (new Point(x - 1, y).Equals(emptyBlk)) //check left
            {
                Swap(blk);
            }
            if (new Point(x, y - 1).Equals(emptyBlk)) //check up
            {
                Swap(blk);
            }
            if (new Point(x, y + 1).Equals(emptyBlk)) //check down
            {
                Swap(blk);
            }
        }

        private void Swap(Block blk)
        {
            Point tmp = new Point(Grid.GetColumn(blk), Grid.GetRow(blk));
            blk.SetValue(Grid.ColumnProperty, (int)emptyBlk.X);
            blk.SetValue(Grid.RowProperty, (int)emptyBlk.Y);
            emptyBlk = tmp;
            steps++;
            CheckResult();
        }

        /// <summary>
        /// Takes action when the block was clicked
        /// </summary>
        /// <param name="sender">The block object</param>
        /// <param name="e">The block event arguments</param>
        public void OnBlockClick(object sender, EventArgs e)
        {
            var blk = (Block)sender;
            CheckSwap(blk);
        }

        private void CheckResult()
        {
            int rightPos = 0;
            foreach (Block blk in grid.Children)
            {
                int val = Int32.Parse(blk.Content as string);
                if (val == GetOneRowPos(blk) + 1)
                    rightPos++;
            }
            if(rightPos == 15)
            {

                AddNewScore addNewScore = new AddNewScore { Score = steps };
                addNewScore.ShowDialog();
            }

        }

        /// <summary>
        /// Converts from 4 rows position to one row positon
        /// </summary>
        /// <param name="blk">The block to find position</param>
        /// <returns></returns>
        private int GetOneRowPos(Block blk)
        {
            int x = Grid.GetColumn(blk);
            int y = Grid.GetRow(blk);
            return x + (y * 4);
        }
    }
}
